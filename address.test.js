'use strict';

process.env['baseUrl'] = 'https://api.landmark.co.uk/bvt/locationsearch/v1/search';
process.env['subscriptionKey'] = '123';
var expect = require( 'chai' ).expect;
var assert = require('assert');
var addressSearch = require( './address' );
var nock = require('nock')


describe( 'Address Search Success Senario', function() {

    [
       {
            "idToken" : "sampleToken",
            "query" : "anderson place"
        }
    ].forEach( function( eventInput ) {

        it( 'Successful Search', function( done ) {
            
            nock(process.env['baseUrl'])
            .get('/address?Subscription-Key=123&query=anderson%20place')
            .reply(200, [{'message':'success'}]);

            var context = {

                succeed: function() {
                    },
                fail: function() {
                    }
            }

            var addressSearchCallback = function(err, result){
                assert.equal(result.status,200,'status code');
                assert.equal(result.payload[0].message,"success",'payload message');
                done();
            }
            addressSearch.get( eventInput , context, addressSearchCallback );
        });

    });


});

describe( 'Address Search failure Senario', function() {

    [
       {
            "idToken" : "InvalidToken",
            "query" : "anderson place"
        }
    ].forEach( function( eventInput ) {

        it( 'Search Failure due to authentication ', function( done ) {

            nock(process.env['baseUrl'])
            .get('/address?Subscription-Key=123&query=anderson%20place')
            .reply(401, {'message':'Not Authorised'});
            
            var context = {

                succeed: function() {
                    },
                fail: function() {
                    }
            }

            var addressSearchCallback = function(err, result){
                assert.equal(result.status,401,'status code');
                assert.equal(result.payload.message,"Not Authorised",'payload message');
                done();
            }

            addressSearch.get( eventInput , context, addressSearchCallback );
        });

    });


});

describe( 'Address Search failure Senario', function() {

    [
       {
            "idToken" :"sampleToken",
            "query" : ""
        }
    ].forEach( function( eventInput ) {

        it( 'Search Failure due to invalid request ', function( done ) {

            nock(process.env['baseUrl'])
            .get('/address?Subscription-Key=123')
            .reply(400, {'message':'Search query cannot be null or empty.'});
            
            var context = {

                succeed: function() {
                    },
                fail: function() {
                    }
            }

            var addressSearchCallback = function(err, result){
                assert.equal(result.status,400,'status code');
                assert.equal(result.payload.message,"Search query cannot be null or empty.",'payload message');
                done();
            }

            addressSearch.get( eventInput , context, addressSearchCallback );
        });

    });


});
    