'use strict';
 const request = require('request');
 
exports.get = function (event, context, callback) {
    
    const baseUrl = process.env.baseUrl;
    const subscriptionKey = process.env.subscriptionKey;

    if(event.idToken != null ){

        const options = buildOptions(baseUrl,subscriptionKey,event.query,event.idToken,'GET');

        request(options, function(error, response, body) {  

            callback( null, buildResponse(response.statusCode,response.body));    
        });  	

    }else{

        callback( null, buildResponse(400,{message:'The request is not valid.'}));
    }
    
};

var buildOptions = function (baseUrl,subscriptionKey,query,AuthToken,requestType){

    var APIUrl = null;

    if(query == ""){

        APIUrl = encodeURI(baseUrl+'/address?Subscription-Key='+
                                 subscriptionKey);
    }else{

        APIUrl = encodeURI(baseUrl+'/address?Subscription-Key='+
                                 subscriptionKey+'&query='+query);
    }
     return {

            method: requestType,
            url:  APIUrl,
            headers: { 'Content-Type' : 'application/json',
                        'Authorization': AuthToken },
            json: true       
        };

}

var buildResponse = function(statuscode,payload) {

     return {
                status: statuscode,
                payload: payload
            };
}


